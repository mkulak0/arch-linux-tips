##### Nemo - File Manager

* nemo

* nemo-ext-git-git - dodatkowe informacje

* nemo-fileroller - do archiwów

* nemo-previewfr

##### Reszta

* menulibre - edytor menu

* cobang - skaner QR

* dconfeditor - masa opcji do konfigurowania gnome

* emoji picker - XD

* extansions - instalowanie extansions

* mark text - Markdown editor 

* telegram-desktop

* fragments - do torrentów

* qnapi - napisy do filmów

* electrum bitcoin - portfel do bitcoina

* digitial assets - kursy kryptowalut

* screenshot

* antimicroX - do remapowania padów

* firefox

* obs studio

* postman

* sysprof - do monitorowania aplikacji

* tor browser

* gmrun - do odpalania aplikacji skrótem klawiaturowym

* fondo - do tapet

* gsconnect - do synchronizacji z KDE connect

* systemadm

* gnome tweaks

* krita

* inkscape

* preload
